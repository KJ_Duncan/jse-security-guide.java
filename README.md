Examples from:  
- Oracle Java Platform, Security Developer's Guide  
- IBM Java Knowledge Center  
- Instant Java Password and Authentication Security  


  
Oracle 2020, Java Platform, Standard Edition, Security Developer's Guide, Release 14, viewed on 12 May 2020, https://docs.oracle.com/en/java/javase/14/security/java-security-overview1.html  
  
Oracle 2019, Secure Coding Guidelines for Java SE, viewed on 21 May 2020, https://www.oracle.com/java/technologies/javase/seccodeguide.html  
  
IBM Corporation 2020, Java Authentication and Authorization Service (JAAS) V2.0, IBM SDK, Java Technology Edition, Version 8, IBM Knowledge Center, viewed on 12 May 2020, https://www.ibm.com/support/knowledgecenter/SSYKE2_8.0.0/com.ibm.java.security.component.80.doc/security-component/JaasDocs/jaas.html  
  
Mayoral, F 2013, Instant Java Password and Authentication Security, Packt Publishing, viewed on 08 June 2020, https://www.packtpub.com/application-development/instant-java-password-and-authentication-security-instant  
  
Common Weakness Enumeration (CWE) 2020, CWE VIEW: Software Development, A Community-Developed List of Software & Hardware Weakness Types, viewed 20 May 2020, https://cwe.mitre.org/data/definitions/699.html  
  
Common Weakness Enumeration (CWE) 2020, CWE VIEW: Architectural Concepts, A Community-Developed List of Software & Hardware Weakness Types, viewed 24 May 2020, https://cwe.mitre.org/data/definitions/1008.html  
  
Google Developers 2020, java.security, Android Developers, viewed 05 June 2020, https://developer.android.com/reference/java/security/package-summary  
  
Google Developers 2020, Cryptography, Android Developers, viewed 05 June 2020, https://developer.android.com/guide/topics/security/cryptography#kotlin  
  
Google Developers 2020, androidx.security.crypto, Android Developers, viewed 05 June 2020, https://developer.android.com/reference/androidx/security/crypto/package-summary  
  
Hexadecimal Dictionary 2020, Hexadecimal converter, viewed 22 June 2020, https://www.hexadecimaldictionary.com/  
  
