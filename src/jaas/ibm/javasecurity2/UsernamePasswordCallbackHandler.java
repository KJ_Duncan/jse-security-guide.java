package jaas.ibm.javasecurity2;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* This class implements a username/password callback handler that gets
   information from the user */
public class UsernamePasswordCallbackHandler implements CallbackHandler {

  /* The handle method does all the work and iterates through the array
     of callbacks, examines the type, and takes the appropriate user
     interaction action. */
  public void handle(Callback[] callbacks) throws UnsupportedCallbackException, IOException {

    for (Callback callback : callbacks) {

      /* Handle username aquisition */
      if (callback instanceof NameCallback) {

        NameCallback nameCallback = (NameCallback) callback;

        System.err.print( nameCallback.getPrompt() );
        System.err.flush();

        nameCallback.setName(new BufferedReader(new InputStreamReader(System.in)).readLine());

        /* Handle password aquisition */
      } else if (callback instanceof PasswordCallback) {

        PasswordCallback passwordCallback = (PasswordCallback) callback;

        System.err.println( passwordCallback.getPrompt() );
        System.err.flush();

        passwordCallback.setPassword(System.console().readPassword());

        /* Other callback types are not handled here */
      } else { throw new UnsupportedCallbackException(callback, "Unsupported Callback Type"); }
    }
  }
}

