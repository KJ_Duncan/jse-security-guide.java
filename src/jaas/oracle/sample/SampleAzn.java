package jaas.oracle.sample;

import javax.security.auth.Subject;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.util.Iterator;

/**
 * @author Oracle 2020, JAAS Authorization Tutorial,
 *         Java Authentication and Authorization Service (JAAS),
 *         Security Developer's Guide, Java Platform,
 *         Standard Edition, Release 14, section 6, viewed 15 May 2020,
 *         https://docs.oracle.com/en/java/javase/14/security/jaas-authorization-tutorial.html
 */
public class SampleAzn {

  public static void main(String[] args) {

    LoginContext loginContext = null;

    try {
      loginContext = new LoginContext("Sample", new MyCallbackHandler());
    } catch (LoginException le) {
      System.err.println("Cannot create LoginContext. " + le.getMessage());
      System.exit(-1);
    } catch (SecurityException se) {
      System.err.println("Cannot create LoginContext. " + se.getMessage());
      System.exit(-1);
    }

    int i;
    for (i = 0; i < 3; i++) {
      try {
        loginContext.login();
        break;

      } catch (LoginException le) {
        System.err.println("Authentication failed:");
        System.err.println("  " + le.getMessage());

        try {
          Thread.currentThread().sleep(3000);
        } catch (Exception e) {
          // ignore
          System.out.println("Thread sleep error" + e.getMessage());
        }
      }
    }

    if (i == 3) {
      System.out.println("Sorry");
      System.exit(-1);
    }

    System.out.println("Authentication succeeded!");

    Subject mySubject = loginContext.getSubject();

    Iterator principalIterator = mySubject.getPrincipals().iterator();

    System.out.println("Authentication user has the following Principals:");

    while (principalIterator.hasNext()) {
      Principal principal = (Principal)principalIterator.next();
      System.out.println("\t" + principal.toString());
    }

    System.out.println("User has " + mySubject.getPublicCredentials().size() + " Public Credential(s)");

    PrivilegedAction action = new SampleAction();
    Subject.doAsPrivileged(mySubject, action, null);

    System.exit(0);
  }
}
