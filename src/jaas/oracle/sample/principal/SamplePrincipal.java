package jaas.oracle.sample.principal;

import java.io.Serializable;
import java.security.Principal;
import java.util.Objects;

/**
 * @author Oracle 2020, SampleLoginModule.java and SamplePrincipal.java,
 *         Java Authentication and Authorization Service (JAAS), Security Developer's Guide,
 *         Java Platform, Standard Edition, Release 14, Section 6, viewed 19 May 2020,
 *         https://docs.oracle.com/en/java/javase/14/security/jaas-authentication-tutorial.html
 */
public class SamplePrincipal implements Principal, Serializable {

  private String name;

  public SamplePrincipal(String name) {
    if (name == null) { throw new NullPointerException("illegal null input"); }
    this.name = name;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public String toString() {
    return "SamplePrincipal{" +
      "name='" + name + '\'' +
      '}';
  }

  @Override
  public boolean equals(final Object o) {
    if (null == o) return false;
    if (this == o) return true;
    if (!(o instanceof SamplePrincipal)) return false;
    final SamplePrincipal that = (SamplePrincipal) o;
    return Objects.equals(this.getName(), that.getName());
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }
}
