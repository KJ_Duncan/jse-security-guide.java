package jaas.oracle.sample;

import java.io.File;
import java.security.PrivilegedAction;

/**
 * @author Oracle 2020, JAAS Authorization Tutorial,
 *         Java Authentication and Authorization Service (JAAS),
 *         Security Developer's Guide, Java Platform,
 *         Standard Edition, Release 14, section 6, viewed 15 May 2020,
 *         https://docs.oracle.com/en/java/javase/14/security/jaas-authorization-tutorial.html
 */
public class SampleAction implements PrivilegedAction {

  @Override
  public Object run() {
    System.out.println("\nYour java.home property: " + System.getProperty("java.home"));
    System.out.println("\nYour user.home property: " + System.getProperty("user.home"));

    File file = new File("foo.txt");

    System.out.print("\nfoo.txt does ");
    if (!file.exists()) System.out.print("not ");
    System.out.println("exist in the current working directory.");

    return null;
  }
}
