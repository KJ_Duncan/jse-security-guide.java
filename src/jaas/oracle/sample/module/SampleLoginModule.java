package jaas.oracle.sample.module;

import jaas.oracle.sample.principal.SamplePrincipal;

import javax.security.auth.Subject;
import javax.security.auth.callback.*;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @author Oracle 2020, SampleLoginModule.java and SamplePrincipal.java,
 *         Java Authentication and Authorization Service (JAAS), Security Developer's Guide,
 *         Java Platform, Standard Edition, Release 14, Section 6, viewed 19 May 2020,
 *         https://docs.oracle.com/en/java/javase/14/security/jaas-authentication-tutorial.html
 */
public class SampleLoginModule implements LoginModule {

  /* Initial state */
  private Subject subject;
  private CallbackHandler callbackHandler;
  private Map sharedState;
  private Map options;

  /* Configurable option */
  private boolean debug = false;

  /* Authentication status */
  private boolean succeeded = false;
  private boolean commitSucceeded = false;

  /* User name and Password */
  private String username;
  private char[] password;

  /* testUser's SamplePrincipal */
  private SamplePrincipal userPrincipal;

  @Override
  public void initialize(final Subject subject,
                         final CallbackHandler callbackHandler,
                         final Map<String, ?> sharedState,
                         final Map<String, ?> options) {
    this.subject = subject;
    this.callbackHandler = callbackHandler;
    this.sharedState = sharedState;
    this.options = options;

    debug = "true".equalsIgnoreCase((String)options.get("debug"));
  }

  @Override
  public boolean login() throws LoginException {

    isCallbackHandlerNull(callbackHandler);

    Callback[] callbacks = new Callback[2];
    callbacks[0] = new NameCallback("user name: ");
    callbacks[1] = new PasswordCallback("password: ", false);

    // ------------------------------------------------------------------------------------------- \\
    try {
      callbackHandler.handle(callbacks);

      username = ((NameCallback)callbacks[0]).getName();

      char[] tmpPassword = ((PasswordCallback)callbacks[1]).getPassword();

      if (tmpPassword == null) { tmpPassword = new char[0]; }

      password = new char[tmpPassword.length];

      System.arraycopy(tmpPassword, 0, password, 0, tmpPassword.length);

      ((PasswordCallback)callbacks[1]).clearPassword();

    } catch (IOException ioe) { throw new LoginException(ioe.toString()); } catch (UnsupportedCallbackException uce) { throw new LoginException("ERROR: " + uce.getCallback().toString() + " not available to garner authentication information from the user."); }
    // ------------------------------------------------------------------------------------------- //

    if (debug) { printUsernameAndPasswordDebug(); }

    boolean usernameCorrect = false;
    boolean passwordCorrect = false;

    if (username.equals("testUser")) { usernameCorrect = true; }

    if (isUserAndPasswordCorrect(usernameCorrect)) {

      passwordCorrect = true;

      if (debug) { printSuccessOrFailureDebug("succeeded"); }

      succeeded = true;

      return true;

    } else {

      if (debug) { printSuccessOrFailureDebug("failed"); }

      succeeded = false;
      username = null;

      clearPassword();

      isUsernameCorrect(!usernameCorrect);

      return false;
    }
  }

  @Override
  public boolean commit() throws LoginException {

    if (succeeded) {

      userPrincipal = new SamplePrincipal(username);

      if (!subject.getPrincipals().contains(userPrincipal)) { subject.getPrincipals().add(userPrincipal); }

      if (debug) { printSuccessOrFailureDebug("added SamplePrincipal to Subject"); }

      username = null;

      clearPassword();

      commitSucceeded = true;

      return true;
    } else { return false; }
  }

  @Override
  public boolean abort() throws LoginException {
    if (succeeded) {
      if (succeeded && !commitSucceeded) {

        succeeded = false;
        username = null;

        clearPassword();

        userPrincipal = null;

      } else { logout(); }
    } else { return false; }
    return true;
  }

  @Override
  public boolean logout() throws LoginException {

    subject.getPrincipals().remove(userPrincipal);

    succeeded = false;
    succeeded = commitSucceeded;
    username = null;

    clearPassword();

    userPrincipal = null;

    return true;
  }

  // ------------------------------------ Helper Functions ---------------------------------------- \\

  private void isCallbackHandlerNull(CallbackHandler callbackHandler) throws LoginException {
    if (callbackHandler == null) { throw new LoginException("ERROR: no CallbackHandler available to garner authentication information from the user."); }
  }

  private boolean isUserAndPasswordCorrect(boolean usernameCorrect) {
    return usernameCorrect && password.length == 12 && password[0] == 't' && password[1] == 'e' && password[2] == 's' && password[3] == 't' && password[4] == 'P' && password[5] == 'a' && password[6] == 's' && password[7] == 's' && password[8] == 'w' && password[9] == 'o' && password[10] == 'r' && password[11] == 'd';
  }

  private void printUsernameAndPasswordDebug() {
    System.out.println("\t\t[SampleLoginModule] user entered user name: " + username);
    System.out.print("\t\t[SampleLoginModule] user entered password: ");

    for (char c : password) { System.out.print(c); }

    System.out.println();
  }

  private void printSuccessOrFailureDebug(String msg) {
    System.out.println("\t\t[SampleLoginModule] authentication " + msg);
  }

  private void isUsernameCorrect(boolean name) throws FailedLoginException {
    if (name) { throw new FailedLoginException("User Name Incorrect"); } else { throw new FailedLoginException("Password Incorrect"); }
  }

  private void clearPassword() {
    if (password != null) {
      Arrays.fill(password, '\u0000');
      password = null;
    }
  }
}
