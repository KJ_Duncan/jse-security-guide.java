package jaas.oracle.sample;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

/**
 *
 * @author Oracle 2020, JAAS Authentication Tutorial,
 *         Java Authentication and Authorization Service (JAAS),
 *         Security Developer's Guide, Java Platform,
 *         Standard Edition, Release 14, section 6, viewed 15 May 2020,
 *         https://docs.oracle.com/en/java/javase/14/security/jaas-authentication-tutorial.html
 */
public class SampleAcn {

  public static void main(String[] args) {
    /* LoginContext needed for authentication */
    LoginContext loginContext = null;

    try {
      /* LoginContext uses the LoginModule ??? */
      loginContext = new LoginContext("Sample", new MyCallbackHandler());
    } catch (LoginException le) {
      System.err.println("Cannot create LoginContext. " + le.getMessage());
      System.exit(-1);
    } catch (SecurityException se) {
      System.err.println("Cannot create LoginContext. " + se.getMessage());
      System.exit(-1);
    }

    /* User has three attempts to get username and password correct */
    int i;
    for (i = 0; i < 3; i++) {
      try {
        loginContext.login();
        break;

      } catch (LoginException le) {
        System.err.println("Authentication failed:");
        System.err.println("  " + le.getMessage());

        try {
          Thread.currentThread().sleep(3000);
        } catch (Exception e) {
          // ignore
          System.out.println("Thread sleep error" + e.getMessage());
        }
      }
    }

    if (i == 3) {
      System.out.println("Sorry");
      System.exit(-1);
    }

    System.out.println("Authentication succeeded!");
  }
}
