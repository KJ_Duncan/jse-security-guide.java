package jaas.oracle.sample;

import javax.security.auth.callback.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This application is text-based.
 * Information is displayed to the user using
 * OutputStreams; System.out, System.err
 *
 * Which gathers input from the user using the
 * InputStream System.in
 */
public class MyCallbackHandler implements CallbackHandler {

  @Override
  public void handle(final Callback[] callbacks) throws IOException, UnsupportedCallbackException {

    for (Callback callback : callbacks) {

      if (callback instanceof TextOutputCallback) {

        TextOutputCallback textOutputCallback = (TextOutputCallback) callback;

        switch (textOutputCallback.getMessageType()) {
          case TextOutputCallback.INFORMATION:
            System.out.println(textOutputCallback.getMessage());
            break;
          case TextOutputCallback.ERROR:
            System.out.println("ERROR: " + textOutputCallback.getMessage());
            break;
          case TextOutputCallback.WARNING:
            System.out.println("WARNING: " + textOutputCallback.getMessage());
            break;
          default:
            throw new IOException("Unsupported message type: " + textOutputCallback.getMessageType());
        }
      } else if (callback instanceof NameCallback) {

        NameCallback nameCallback = (NameCallback) callback;

        System.err.println(nameCallback.getPrompt());
        System.err.flush();

        nameCallback.setName(new BufferedReader(new InputStreamReader(System.in)).readLine());

      } else if (callback instanceof PasswordCallback) {

        PasswordCallback passwordCallback = (PasswordCallback) callback;

        System.err.println(passwordCallback.getPrompt());
        System.err.flush();

        passwordCallback.setPassword(System.console().readPassword());

      } else { throw new UnsupportedCallbackException(callback, "Unrecognised Callback"); }
    }
  }
}
