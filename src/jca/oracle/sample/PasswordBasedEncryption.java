import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

public class PasswordBasedEncryption {
  private SecureRandom secureRandom;
  private PBEParameterSpec pbeParameterSpec;
  private PBEKeySpec pbeKeySpec;
  private SecretKeyFactory keyFactory;
  private SecretKey pbeKey;
  private Cipher pbeCipher;


  byte[] createSalt(SecureRandom random) {
    // Size 32 bytes for SHA-256
    byte[] salt = new byte[32];
    // A randomised salt
    random.nextBytes(salt);
    return salt;
  }

  PBEKeySpec userPassword() throws NoSuchAlgorithmException {
    // minimum iteration count for PBE Algorithms
    int iterations = 1000;

    // throws NoSuchAlgorithmException
    secureRandom = SecureRandom.getInstanceStrong();

    byte[] salt = createSalt(secureRandom);

    // Create PBE parameter set
    pbeParameterSpec = new PBEParameterSpec(salt, iterations);

    /* Prompt user for encryption password.
       Collect user password as char array, and convert
       it into a SecretKey object, using a PBE key factory. */
    char[] password = System.console().readPassword("Enter encryption password: ");

    pbeKeySpec = new PBEKeySpec(password);

    // GC will swallow
    Arrays.fill(password, '\u0000');
    password = null;

    return pbeKeySpec;
  }

  byte[] encryptCleartext(String cleartext) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

    // throws NoSuchAlgorithmException
    keyFactory = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_256");

    // throws InvalidKeySpecException
    // Convert PBEKeySpec into a SecretKey object, using a PBE key factory.
    pbeKey = keyFactory.generateSecret(userPassword());

    // throws NoSuchPaddingException
    // Create PBE Cipher
    pbeCipher = Cipher.getInstance("PBEWithHmacSHA256AndAES_256");

    // throws InvalidAlgorithmParameterException, InvalidKeyException
    // Initialise PBE Cipher with key and parameters
    pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParameterSpec);

    // The cleartext to byte array
    byte[] cleartextAsBytes = cleartext.getBytes();

    // throws BadPaddingException, IllegalBlockSizeException
    // Encrypt the cleartext
    byte[] ciphertext = pbeCipher.doFinal(cleartextAsBytes);

    // Return for storage
    return ciphertext;
  }
}
