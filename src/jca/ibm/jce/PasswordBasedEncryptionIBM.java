import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

/*
 * You should always collect and store security sensitive information in a char array instead.
 * For that reason, the javax.crypto.spec.PBEKeySpec class takes (and returns) a password as a char array.
 * The following method is an example of how to collect a user password as a char array:
 *
 * @author IBM 2020, Using Password-Based Encryption, IBM Knowledge Center, viewed on 02 June 2020,
 *         https://www.ibm.com/support/knowledgecenter/SSYKE2_8.0.0/com.ibm.java.security.component.80.doc/security-component/JceDocs/api_ex_pbe.html
 */
public class PasswordBasedEncryptionIBM {

  /* Reads user password from given input stream. */
  public char[] readPassword(InputStream in) throws IOException {
    char[] lineBuffer;
    char[] buf;
    int i;

    buf = lineBuffer = new char[128];

    int room = buf.length;
    int offset = 0;
    int c;

    loop:
    while (true) {
      switch (c = in.read()) {
        case -1:
        case '\n':
          break loop;

        case '\r':
          int c2 = in.read();
          if ((c2 != '\n') && (c2 != -1)) {
            if (!(in instanceof PushbackInputStream)) {
              in = new PushbackInputStream(in);
            }
            ((PushbackInputStream) in).unread(c2);
          } else
            break loop;

        default:
          if (--room < 0) {
            buf = new char[offset + 128];
            room = buf.length - offset - 1;
            System.arraycopy(lineBuffer, 0, buf, 0, offset);
            Arrays.fill(lineBuffer, ' ');
            lineBuffer = buf;
          }
          buf[offset++] = (char) c;
          break;
      }
    }

    if (offset == 0) {
      return null;
    }

    char[] ret = new char[offset];
    System.arraycopy(buf, 0, ret, 0, offset);
    Arrays.fill(buf, ' ');

    return ret;
  }

  /* In order to use Password-Based Encryption (PBE) as defined in PKCS#5,
     we have to specify a salt and an iteration count.
     The same salt and iteration count that are used for encryption must be used for decryption: */
  public void prompt() throws IOException,
    NoSuchAlgorithmException,
    InvalidKeySpecException,
    NoSuchPaddingException,
    InvalidAlgorithmParameterException,
    InvalidKeyException,
    BadPaddingException,
    IllegalBlockSizeException {

    // Salt https://www.hexadecimaldictionary.com/hexadecimal/0xC7/
    byte[] salt = {
      //     199,        115,         33,        140,
      (byte) 0xc7, (byte) 0x73, (byte) 0x21, (byte) 0x8c,
      //     126,        200,        238,        153
      (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99
    };

    // Iteration count, jse-security-guide suggests minimum 1000 iterations
    int count = 20;

    // Create PBE parameter set
    PBEParameterSpec pbeParamSpec = new PBEParameterSpec(salt, count);

    /* Prompt user for encryption password.
     Collect user password as char array (using the
     "readPassword" method from earlier code), and convert
     it into a SecretKey object, using a PBE key factory. */
    System.out.print("Enter encryption password: ");
    System.out.flush();

    // throws IOException
    PBEKeySpec pbeKeySpec = new PBEKeySpec(readPassword(System.in));

    // throws NoSuchAlgorithmException
    SecretKeyFactory keyFac = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_128");

    // throws InvalidKeySpecException
    SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

    // Create PBE Cipher, throws NoSuchPaddingException
    Cipher pbeCipher = Cipher.getInstance("PBEWithHmacSHA256AndAES_128");

    // Initialize PBE Cipher with key and parameters, throws InvalidAlgorithmParameterException, InvalidKeyException
    pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

    // Our cleartext
    byte[] cleartext = "Credentials(key=someKey, value=someValue)".getBytes();

    // Encrypt the cleartext, throws BadPaddingException, IllegalBlockSizeException
    byte[] ciphertext = pbeCipher.doFinal(cleartext);
  }
}
