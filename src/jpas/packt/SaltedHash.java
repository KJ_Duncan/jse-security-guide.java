package jpas.packt;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * @author Mayoral, F 2013, Instant Java Password and Authentication Security,
 *         Packt Publishing, viewed on 08 June 2020,
 *         https://www.packtpub.com/application-development/instant-java-password-and-authentication-security-instant
 */
public class SaltedHash {
  private User user;

  // a fabrication of a persistent database
  private class User {
    private String username;
    private String password;
    private String salt;

    void setUsername(String name) { this.username = name; }
    void setPassword(String pword) { this.password = pword; }
    void setSalt(String salt) { this.salt = salt; }

    String getName() { return this.username; }
    String getPassword() { return this.password; }
    String getSalt() { return this.salt; }
  }

  public void saveUser(User u) {
    user = u;
  }

  public User retrieveUser(String login) {
    return user;
  }

  public byte[] getSalt() throws NoSuchAlgorithmException {

    /* Secure random number generator */
    SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");

    /* salt of 16 bytes, 8 bits to 1 byte: 8 * 16 = 128 bit salt strength */
    byte[] salt = new byte[16];

    /* salt shaker */
    secureRandom.nextBytes(salt);
    return salt;
  }

  public byte[] getSaltedHashSHA512(String password, byte[] salt) {
    try {

      /* A sha-512 MessageDigest instance */
      MessageDigest md = MessageDigest.getInstance("SHA-512");

      /* Add the salt to the digest */
      md.update(salt);

      /* Digest the password as a byte array, internally concatenates salt:password */
      byte byteData[] = md.digest(password.getBytes());

      /* Reset the MessageDigest, done internally */
      md.reset();

      /* The hashed password */
      return byteData;
    }
    catch (NoSuchAlgorithmException ex) {
      Logger.getLogger("SHA-512").log(Level.SEVERE, "SHA-512 is not a valid algorithm name", ex);
      return null;
    }
  }

  /* Converts a hexadecimal string to a byte array */
  public byte[] fromHex(String hex) {

    /* Create a byte array with half of the hex string length */
    byte[] binary = new byte[hex.length() / 2];

    /*For 0 to byte array length */
    for (int i = 0; i < binary.length; i++) {

      /* Parse 2 chars from base 16 to base 2 */
      //                                                2 * 0, 2 * 0 + 2
      //                                                2 * 1, 2 * 1 + 2
      //                                                2 * 2, 2 * 2 + 2
      //                                                2 * 3, 2 * 3 + 2
      //           base 2                               2 * 4, 2 * 4 + 2  from base 16
      binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
    }
    return binary;
  }

  /* Converts a byte array to a hexadecimal string */
  public String toHex(byte[] array) {

    /* Create a new BigInteger with the byte array */
    BigInteger bi = new BigInteger(1, array);

    /* Get the big integer as a string */
    String hex = bi.toString(16);

    /* Calculate the padding length */
    int paddingLength = (array.length * 2) - hex.length();

    /* If there is any padding */
    if (paddingLength > 0) {

      /* Format the padding length and concatenate the hex string
                 number of 0s is paddingLength as digits ++ hex */
      return String.format("%0" + paddingLength + "d", 0) + hex;
    }
    else {
      return hex;
    }
  }

  /* Validate a users login attempt */
  public boolean isValidUser(String login, String password) throws IOException {

    /* Retrieve the user from the database */
    User user = retrieveUser(login);

    /* Get the salt used with the password when the user was created */
    String strOriginalSalt = user.getSalt();

    /* Get the bytes from the salt, remember its in hex format */
    byte[] byteSalt = fromHex(strOriginalSalt);

    /* Digest the login password with the orginal salt */
    byte[] loginPassword = getSaltedHashSHA512(password, byteSalt);

    /* Get the stored password for comparing */
    byte[] storedPassword = fromHex(user.getPassword());

    /* Compare the incoming password with the stored password */
    boolean result = Arrays.equals(loginPassword, storedPassword);

    if (result) System.out.println("Successfull login!");
    else System.out.println("Login failed!");

    return result;
  }

  public void createUser(String login, String password) {

    /* byte array to save the salt */
    byte[] byteSalt = null;
    try {
      byteSalt = getSalt();
    }
    catch (NoSuchAlgorithmException ex) {
      Logger.getLogger(SaltedHash.class.getName()).log(Level.SEVERE, null, ex);
    }

    /* Digest the password with the salt */
    byte[] byteDigestPassword = getSaltedHashSHA512(password, byteSalt);

    /* Get the hased password in Base 64 */
    String strDigestPassword = toHex(byteDigestPassword);

    /* Get the hased salt in Base 64 */
    String strSalt = toHex(byteSalt);

    // a fabrication
    User user = new User();
    user.setUsername(login);

    /* the hashed password */
    user.setPassword(strDigestPassword);

    /* In order to generate this salted hash again to compare, we will need the original salt */
    user.setSalt(strSalt);
    saveUser(user);
  }
}
