/*
 * Although MD5 is a widely used hashing algorithm, it is far from being secure since MD5 generates fairly weak hashes.
 * MD5 is a cryptographic hash function that produces a 128-bit hash value (32 characters in length).
 *
 * The advantages of MD5 hashes are as follows:
 *  - Easy to implement
 *  - Very fast in execution and cost-effective in resources
 *
 * The disadvantages of MD5 hashes are as follows:
 *  - MD5 hashes are not collision resistant. This means different passwords can eventually result in the same hash
 *  - Since it's fast in execution, it's susceptible to brute force and dictionary attacks
 *  - Rainbow tables with words and generated hashes allow very quick searches for a known hash and also get the original word quickly
 *
 * MD5 is useful to check Big Data consistency and it's better than plain text,
 * but it's not a good option to keep really sensitive data (such as passwords) safe.
 *
 * @author Mayoral, F 2013, Instant Java Password and Authentication Security,
 *         Packt Publishing, viewed on 08 June 2020,
 *         https://www.packtpub.com/application-development/instant-java-password-and-authentication-security-instant
 */
public class HashMD5 {

  public static String getHashMD5(String password) {
    try {

      /* Get the MessageDigest instance */
      MessageDigest md = MessageDigest.getInstance("MD5");

      /* Add the value's bytes to the MessageDigest */
      md.update(password.getBytes());

      /* Execute the digest method to get the hash's bytes */
      byte byteData[] = md.digest();

      /* The byteData array contains the hash bytes in decimal format
         in order to represent that hash as a String, we need to encode
         this byteData array into Hexadecimal format.
         We need a StringBuilder to convert every single byte to a
         Hexadecimal encoded String. */
      StringBuilder sb = new StringBuilder();

      // For each byte
      for (int i = 0; i < byteData.length; i++) {

        /* Encode the byte to hexadecimal format and append to the String builder */
        sb.append(Integer.toString((byteData[i] & 0xFF) + 0x100, 16).substring(1));
      }

      /* To get the hashed password, get the String builder buffered values as a String
         A variable now contains the password's hash as a String */
      return sb.toString();
    }
    catch (NoSuchAlgorithmException ex) {
      Logger.getLogger("MD5").log(Level.SEVERE, null, ex);
      return null;
    }
  }

  public static void main(String[] args) {
    String password = "www.packetpub.com";
    System.out.println("Original value: " + password);
    System.out.println("MD5 Hash: " + getHashMD5(password));
    System.out.flush();
  }
}
