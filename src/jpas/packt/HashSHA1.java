/*
 * SHA-1 is a cryptographic hash function that produces a 160-bit hash value (40 characters in length).
 *
 * @author Mayoral, F 2013, Instant Java Password and Authentication Security,
 *         Packt Publishing, viewed on 08 June 2020,
 *         https://www.packtpub.com/application-development/instant-java-password-and-authentication-security-instant
 */
public class HashSHA1 {

  public static String getHashSHA1(String password) {
    try {

      /* Get the MessageDigest instance */
      MessageDigest md = MessageDigest.getInstance("SHA-1");

      /* Add the value's bytes to the MessageDigest */
      md.update(password.getBytes());

      /* Execute the digest method to get the hash's bytes */
      byte byteData[] = md.digest();

      /* The byteData array contains the hash bytes in decimal format
         in order to represent that hash as a String, we need to encode
         this byteData array into Hexadecimal format.
         We need a StringBuilder to convert every single byte to a
         Hexadecimal encoded String. */
      StringBuilder sb = new StringBuilder();

      // For each byte
      for (int i = 0; i < byteData.length; i++) {

        /* Encode the byte to hexadecimal format and append to the String builder
                   (yields 1 if both bit are 1) &  255  +   256
                                           byte & 11_111_111 + 100_000_000 */
        sb.append(Integer.toString((byteData[i] & 0xFF) + 0x100, 16).substring(1));
      }

      /* To get the hashed password, get the String builder buffered values as a String
         A variable now contains the password's hash as a String */
      return sb.toString();
    }
    catch (NoSuchAlgorithmException ex) {
      Logger.getLogger("SHA-1").log(Level.SEVERE, null, ex);
      return null;
    }
  }

  public static void main(String[] args) {
    String password = "www.packetpub.com";
    System.out.println("Original value: " + password);
    System.out.println("SHA-1 Hash: " + getHashSHA1(password));
    System.out.flush();
  }
}
