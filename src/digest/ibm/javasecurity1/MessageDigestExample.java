import java.security.*;
import javax.crypto.*;

/*
 * @author Rubin, B 2002, Message digest code example,
 *         Java security: Java security, Part 1: Crypto basics,
 *         IBM developerWorks, IBM Corporation 2002, viewed 21 May 2020,
 *         https://www.ibm.com/developerworks/java/tutorials/j-sec1/j-sec1-pdf.pdf
 */
public class MessageDigestExample {

  public static void main(String[] args) throws Exception {

    if (args.length != 1) { System.err.println("Usage: java MessageDigestExample text"); System.exit(-1); }

    byte[] plaintext = args[0].getBytes("UTF8");

    /* get the message digest object using the md5 algorithm */
    MessageDigest messageDigest = MessageDigest.getInstance("MD5");

    /* print the provider used */
    System.out.println("\n" + messageDigest.getProvider().getInfo());

    /* calculate the digest */
    messageDigest.update(plaintext);

    /* print it out */
    System.out.println("\nDigest: ");
    System.out.println(new String(messageDigest.digest(), "UTF8"));
  }
}
